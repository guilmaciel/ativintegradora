package anoBissexto;

import static org.junit.Assert.*;

import org.junit.Test;

public class TesteAnoBissexto {

	@Test
	public void testeAno1() {
		assertEquals(false, AnoBissexto.isAnoBissexto(1));
	}
	
	
	@Test
	public void testeAno2() {
		assertEquals(false, AnoBissexto.isAnoBissexto(2));
	}
	@Test
	public void testeAno3() {
		assertEquals(false, AnoBissexto.isAnoBissexto(3));
	}
	
	@Test
	public void testeAno4() {
		assertEquals(true, AnoBissexto.isAnoBissexto(4));
	}
	@Test
	public void testeAno1600() {
		assertEquals(true, AnoBissexto.isAnoBissexto(1600));
	}
	@Test
	public void testeAno1900() {
		assertEquals(false, AnoBissexto.isAnoBissexto(1900));
	}
	@Test
	public void testeAno1732() {
		assertEquals(true, AnoBissexto.isAnoBissexto(1732));
	}
	@Test
	public void testeAno1889() {
		assertEquals(false, AnoBissexto.isAnoBissexto(1889));
	}
	
	
	
	
	
}
